#!/bin/bash
##################################################################################################
##                                                                                              ##
##     ...............        .....                                                             ##
##    .doXMMMMxo;oMMMMO  .No KMMMMxdxOd.                                                        ##
##       kMMMM.   lMMMMO'N:  KMMMM.   dX                                                        ##
##       kMMMM.    ,WMMMW,   KMMMM.   cN                                                        ##
##       kMMMM.     .NMX.    KMMMM:;:x0.    Bei Fragen, komm einfach in den Matrix-Space:       ##
##       kMMMM.      00      KMMMMc:,.      https://matrix.to/#/#sontypiminternet:matrix.org    ##
##       :dddd      ;o       ldddd              So'n Typ Im Internet (2022)                     ##
##                                                                                              ##
##################################################################################################

# Prüfen, dass ich die neusten Dateien habe:
echo "Überprüfe neuste Dateien..."
git pull

# Jetzt werden alle Dateien in meinem Projektordner gesichert:
git add --all .

# Hier Commit-Kommentar einfügen
echo "####################################"
echo "Schreibe dein Commit-Kommentar"
echo "####################################"

read input

# Commit wird ausgeführt mit deinem Commit-Kommentar

git commit -m "$input"

# Lokales Repository wird hochgeladen

git push


echo "################################################################"
echo "###################   Git Push erledigt  #######################"
echo "################################################################"